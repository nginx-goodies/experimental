#ifndef _NGX_HTTP_FLYNX_SYNCHRONISATION_INCLUDED_
#define _NGX_HTTP_FLYNX_SYNCHRONISATION_INCLUDED_

/*
  Code in ngx_flynx_synchronisation.h/.c inspired from 
  https://github.com/yzprofile/ngx_http_dyups_module
*/

#include <ngx_core.h>
#include "ngx_http_flynx_common.h"

ngx_int_t ngx_http_flynx_get_shm_name(ngx_str_t *shm_name, ngx_pool_t *pool, ngx_uint_t generation);
char * ngx_http_flynx_init_shm(ngx_conf_t *cf, void *conf);
ngx_int_t ngx_http_flynx_init_shm_zone(ngx_shm_zone_t *shm_zone, void *data);

void ngx_http_flynx_timer_callback(ngx_event_t *ev);
void ngx_http_flynx_timer_callback_locked(ngx_event_t *ev);
ngx_int_t ngx_http_flynx_send_msg(ngx_flynx_data_t *content);
ngx_int_t ngx_http_flynx_send_msg_with_cycle(ngx_flynx_data_t *content, volatile ngx_cycle_t *cycle);
void ngx_http_flynx_destroy_msg(ngx_slab_pool_t *shpool, ngx_flynx_msg_t *msg);
void ngx_http_flynx_destroy_msg_locked(ngx_slab_pool_t *shpool, ngx_flynx_msg_t *msg);

ngx_int_t ngx_http_flynx_data_init_handler(void *data);

#endif // _NGX_HTTP_FLYNX_SYNCHRONISATION_INCLUDED_