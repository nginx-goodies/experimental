#ifndef _NGX_HTTP_FLYNX_UPSTREAM_INCLUDED_
#define _NGX_HTTP_FLYNX_UPSTREAM_INCLUDED_

#include "ngx_http_flynx_common.h"

ngx_http_upstream_main_conf_t * ngx_http_flynx_upstream_main_conf(ngx_http_request_t *r);
ngx_http_upstream_srv_conf_t * ngx_http_flynx_upstream_config(ngx_http_request_t *r, ngx_str_t *host);

ngx_http_upstream_rr_peers_t * ngx_http_flynx_merge_peers(
  ngx_http_upstream_rr_peers_t *peers, 
  ngx_http_upstream_rr_peer_t *peer, 
  ngx_uint_t peer_number,
  ngx_addr_t *addrs,
  ngx_uint_t addrs_number,
  ngx_flynx_data_peer_defaults_t *defaults);

ngx_int_t ngx_http_flynx_data_updown_handler(void *data);
ngx_int_t ngx_http_flynx_data_add_peer_handler(void *data);
ngx_int_t ngx_http_flynx_data_delete_peer_handler(void *data);
ngx_int_t ngx_http_flynx_data_replace_peer_handler(void *data);
void ngx_http_flynx_data_peer_list_alloc(ngx_slab_pool_t *shpool, void *original, void *data);
void ngx_http_flynx_data_peer_list_free(ngx_slab_pool_t *shpool, void *data);

ngx_int_t ngx_flynx_sockaddr_equal(struct sockaddr *a, struct sockaddr *b);

#endif //_NGX_HTTP_FLYNX_UPSTREAM_INCLUDED_