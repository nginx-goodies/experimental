#include "ngx_http_flynx_common.h"
#include "ngx_http_flynx_synchronisation.h"
#include "ngx_http_flynx_management.h"

#if NGX_HTTP_FLYNX_HAVE_LUA
#include "lua/ngx_http_flynx_lua.h"
#endif

#include <jansson.h>

ngx_flynx_global_ctx_t ngx_flynx_global_ctx;
ngx_int_t ngx_http_flynx_generation = 0;

static void * ngx_http_flynx_malloc(size_t size);
static void ngx_http_flynx_free(void *p);


static ngx_int_t ngx_http_flynx_init(ngx_conf_t *cf);
static char * ngx_http_flynx_management (ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
static void * ngx_http_flynx_create_main_conf(ngx_conf_t *cf);
static char * ngx_http_flynx_init_main_conf(ngx_conf_t *cf, void *conf);

static void * ngx_http_flynx_create_server_conf(ngx_conf_t *cf);
static char * ngx_http_flynx_merge_server_conf(ngx_conf_t *cf, void *prev, void *conf);
static void * ngx_http_flynx_create_location_conf(ngx_conf_t *cf);
static char * ngx_http_flynx_merge_location_conf(ngx_conf_t *cf, void *prev, void *conf);

static ngx_int_t ngx_http_flynx_init_master(ngx_log_t *log);
static ngx_int_t ngx_http_flynx_init_module(ngx_cycle_t *cycle);
static ngx_int_t ngx_http_flynx_init_process(ngx_cycle_t *cycle);
static void ngx_http_flynx_exit_process(ngx_cycle_t *cycle);

  
static ngx_http_module_t ngx_http_flynx_module_ctx = {
    NULL,                          /* preconfiguration */
    ngx_http_flynx_init,            /* postconfiguration */

    ngx_http_flynx_create_main_conf,                          /* create main configuration */
    ngx_http_flynx_init_main_conf,                          /* init main configuration */

    ngx_http_flynx_create_server_conf,                          /* create server configuration */
    ngx_http_flynx_merge_server_conf,                          /* merge server configuration */

    ngx_http_flynx_create_location_conf,  /* create location configuration */
    ngx_http_flynx_merge_location_conf /* merge location configuration */
};

static ngx_command_t ngx_http_flynx_commands[] =  {
  {
    ngx_string("flynx_management"),
    NGX_HTTP_LOC_CONF | NGX_CONF_1MORE,
    ngx_http_flynx_management,
    NGX_HTTP_LOC_CONF_OFFSET,
    0,
    NULL },
  ngx_null_command
};

ngx_module_t ngx_http_flynx_module = {
    NGX_MODULE_V1,
    &ngx_http_flynx_module_ctx,    /* module context */
    ngx_http_flynx_commands,       /* module directives */
    NGX_HTTP_MODULE,               /* module type */
    ngx_http_flynx_init_master,    /* init master */
    ngx_http_flynx_init_module,    /* init module */
    ngx_http_flynx_init_process,   /* init process */
    NULL,                          /* init thread */
    NULL,                          /* exit thread */
    ngx_http_flynx_exit_process,   /* exit process */
    NULL,                          /* exit master */
    NGX_MODULE_V1_PADDING
};

static void * ngx_http_flynx_malloc(size_t size) {
  return ngx_alloc(size, ngx_cycle->log);
}

static void ngx_http_flynx_free(void *p) {
  ngx_free(p);
}

static char *
ngx_http_flynx_management (ngx_conf_t *cf, ngx_command_t *cmd, void *conf) {
  ngx_int_t rc;
  ngx_http_core_loc_conf_t  *clcf;
  ngx_http_flynx_loc_conf_t *flcf;
  ngx_str_t *value;
  ngx_str_t addr;
  ngx_cidr_t cidr;
  ngx_int_t flag_set = 0;
    
  ngx_memzero(&cidr, sizeof(ngx_cidr_t));
  
  clcf = ngx_http_conf_get_module_loc_conf(cf, ngx_http_core_module);
  flcf = conf;
  value = cf->args->elts;
  
#if (NGX_PCRE)
  if(clcf->regex) {
    ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
             "flynx_management is unsupported in regexp locations. Please use a prefix location, like location /flynx { }");
             
    return NGX_CONF_ERROR;
  }
#endif

  clcf->handler = ngx_http_flynx_management_handler;
  
  if(ngx_strcasecmp(value[1].data, (u_char *) "off") == 0) {
    flcf->management = NGX_FLYNX_MANAGEMENT_OFF;
    flag_set = 1;
  }
    
  if(ngx_strcasecmp(value[1].data, (u_char *) "on") == 0) {
    flcf->management = NGX_FLYNX_MANAGEMENT_ON;
    flag_set = 1;
  }
  
  if(ngx_strcasecmp(value[1].data, (u_char *) "readonly") == 0) {
    flcf->management = NGX_FLYNX_MANAGEMENT_READONLY;
    flag_set = 1;
  }
  
  if(!flag_set) {
    ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
      "first parameter to %V must either be 'on', 'off' or 'readonly'. Received: '%V'.", &value[0], &value[1]);
                       
    return NGX_CONF_ERROR;
  }
  
  if(flcf->management) {
    if(cf->args->nelts == 2) {
      ngx_str_set(&addr, "127.0.0.1");
    } else {
      addr = value[2];
    }
    rc = ngx_ptocidr(&addr, &cidr);

    if (rc == NGX_ERROR) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                     "invalid parameter \"%V\", defaulting to 127.0.0.1", &addr);
        
        ngx_str_set(&addr, "127.0.0.1");
        ngx_ptocidr(&addr, &cidr);
    }

    if (rc == NGX_DONE) {
        ngx_conf_log_error(NGX_LOG_WARN, cf, 0,
                     "low address bits of %V are meaningless", &addr);
    }
    
    flcf->management_allow = cidr;
  }

  
  return NGX_CONF_OK;
}

static ngx_int_t 
ngx_http_flynx_init(ngx_conf_t *cf) {
#if NGX_HTTP_FLYNX_HAVE_LUA
  ngx_http_lua_add_package_preload(cf, "flynx", ngx_http_flynx_register_lua);
#endif
    
  return NGX_OK;
}

static void *
ngx_http_flynx_create_main_conf(ngx_conf_t *cf) {
  ngx_http_flynx_main_conf_t *conf;
  
  conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_flynx_main_conf_t));
  
  if(conf == NULL) {
    return NGX_CONF_ERROR;
  }
  
  return conf;
}

static char *
ngx_http_flynx_init_main_conf(ngx_conf_t *cf, void *conf) {
  ngx_http_flynx_main_conf_t *fmcf = conf;
  
  fmcf->shm_size = 2*1024*1024;

  ngx_http_flynx_generation++;
  
  return ngx_http_flynx_init_shm(cf, conf);
}

static void *
ngx_http_flynx_create_server_conf(ngx_conf_t *cf) {
  ngx_http_flynx_srv_conf_t *conf;
  
  conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_flynx_srv_conf_t));
  
  if(conf == NULL) {
    return NGX_CONF_ERROR;
  }
  
  return conf;
}

static char *
ngx_http_flynx_merge_server_conf(ngx_conf_t *cf, void *prev, void *conf) {
  return NGX_CONF_OK;
}

static void *
ngx_http_flynx_create_location_conf(ngx_conf_t *cf) {
  ngx_http_flynx_loc_conf_t *conf;
  
  conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_flynx_loc_conf_t));
  
  if(conf == NULL) {
    return NGX_CONF_ERROR;
  }
  
  conf->management = NGX_FLYNX_MANAGEMENT_OFF;
  
  return conf;
}

static char *
ngx_http_flynx_merge_location_conf(ngx_conf_t *cf, void *prev, void *conf) {
  ngx_http_flynx_loc_conf_t *parent = prev;
  ngx_http_flynx_loc_conf_t *child = conf;
  
  if(child->management == NGX_CONF_UNSET) {
    child->management = parent->management;
  }

  return NGX_CONF_OK;
}

static ngx_int_t
ngx_http_flynx_init_master(ngx_log_t *log) {
  return NGX_OK;  
}

static ngx_int_t
ngx_http_flynx_init_module(ngx_cycle_t *cycle) {
  ngx_flynx_data_init_t p;

  ngx_flynx_data_t content = ngx_flynx_data_init(&p, 1);
  ngx_http_flynx_send_msg_with_cycle(&content, cycle);
  
  return NGX_OK;
}

static ngx_int_t
ngx_http_flynx_init_process(ngx_cycle_t *cycle) {
  json_set_alloc_funcs(ngx_http_flynx_malloc, ngx_http_flynx_free);
  ngx_event_t *timer;

  timer = &ngx_flynx_global_ctx.msg_timer;
  ngx_memzero(timer, sizeof(ngx_event_t));

  timer->handler = ngx_http_flynx_timer_callback;
  timer->log = cycle->log;
  
  ngx_add_timer(timer, 1000);
 

  return NGX_OK;
}

static void
ngx_http_flynx_exit_process(ngx_cycle_t *cycle) {
} 