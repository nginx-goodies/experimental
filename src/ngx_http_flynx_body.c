#include "ngx_http_flynx_body.h"

ngx_buf_t *
ngx_http_flynx_read_body(ngx_http_request_t *r)
{
  if (r->request_body->temp_file) {
    return ngx_http_flynx_read_body_from_file(r);
  }

  size_t        len;
  ngx_buf_t    *buf, *next, *body;
  ngx_chain_t  *cl;

  ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                 "[flynx] interface read post body");

  cl = r->request_body->bufs;
  buf = cl->buf;

  if (cl->next == NULL) {
    return buf;
  } else {
    next = cl->next->buf;
    len = (buf->last - buf->pos) + (next->last - next->pos);

    body = ngx_create_temp_buf(r->pool, len);
    if (body == NULL) {
        return NULL;
    }

    body->last = ngx_cpymem(body->last, buf->pos, buf->last - buf->pos);
    body->last = ngx_cpymem(body->last, next->pos, next->last - next->pos);
  }

  return body;
}

ngx_buf_t *
ngx_http_flynx_read_body_from_file(ngx_http_request_t *r)
{
  size_t        len;
  ssize_t       size;
  ngx_buf_t    *buf, *body;
  ngx_chain_t  *cl;

  ngx_log_debug0(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                 "[flynx] interface read post body from file");

  len = 0;
  cl = r->request_body->bufs;

  while (cl) {
    buf = cl->buf;

    if (buf->in_file) {
        len += buf->file_last - buf->file_pos;

    } else {
        len += buf->last - buf->pos;
    }

    cl = cl->next;
  }

  ngx_log_debug1(NGX_LOG_DEBUG_HTTP, r->connection->log, 0,
                 "[flynx] interface read post body file size %ui", len);

  body = ngx_create_temp_buf(r->pool, len);
  if (body == NULL) {
      return NULL;
  }

  cl = r->request_body->bufs;

  while (cl) {
    buf = cl->buf;

    if (buf->in_file) {

        size = ngx_read_file(buf->file, body->last,
                             buf->file_last - buf->file_pos, buf->file_pos);

        if (size == NGX_ERROR) {
            return NULL;
        }

        body->last += size;

    } else {
        body->last = ngx_cpymem(body->last, buf->pos, buf->last - buf->pos);
    }

    cl = cl->next;
  }

  return body;
}

ngx_array_t * 
ngx_flynx_parse_body(ngx_pool_t *pool, ngx_buf_t *input) {
  u_char *p, *first, *last, *temp;
  ngx_int_t feeds;
  ngx_int_t i;
  
  p = input->pos;
  first = p;
  last = input->last;
  feeds = 0;
  
  ngx_array_t *arr = ngx_array_create(pool, 1, sizeof(ngx_str_t));
  
  while(p <= last) {    
    if(p < last && (*p != CR) && (*p != LF)) {
      p++;
      continue;
    }
    
    if(p != last) {
      if(*p == CR || *p == LF) {
        feeds++;
         
        temp = p+1;
        if(*temp == CR || *temp == LF) {
          feeds++;
        }
      }
    }
    
    ngx_str_t *cur = ngx_array_push(arr);
    cur->data = ngx_pcalloc(pool, p-first);
    ngx_memcpy(cur->data, first, p - first);
    cur->len = p - first;
  
    ngx_log_error(NGX_LOG_ERR, ngx_cycle->log, 0,
       "domain: %V, length: %d", cur, cur->len);
    
    for(i = 0; i < feeds; i++) {
      p++;
    }
    first = p;
    feeds = 0;
    
    ngx_log_error(NGX_LOG_ERR, ngx_cycle->log, 0,
     "end: first: %p p: %p last: %p", first, p, last);
     
    if(p == last) {
      break;
    }
  }
  
  return arr;
}

ngx_array_t *
ngx_flynx_parse_path(ngx_pool_t *pool, ngx_str_t *path, ngx_str_t *loc_path) {
  ngx_array_t *path_array = ngx_flynx_parse_single_path(pool, path);
  ngx_array_t *loc_array = ngx_flynx_parse_single_path(pool, loc_path);
  
  ngx_str_t *elts = path_array->elts;
  elts += loc_array->nelts;
  path_array->elts = elts;
  
  path_array->nelts -= loc_array->nelts;
  
  return path_array;
}

ngx_array_t *
ngx_flynx_parse_single_path(ngx_pool_t *pool, ngx_str_t *path)
{
    u_char       *p, *last, *end;
    ngx_str_t    *str;
    ngx_array_t  *array;

    array = ngx_array_create(pool, 8, sizeof(ngx_str_t));
    if (array == NULL) {
        return NULL;
    }

    p = path->data + 1;
    last = path->data + path->len;

    while(p < last) {
        end = ngx_strlchr(p, last, '/');
        str = ngx_array_push(array);

        if (str == NULL) {
            return NULL;
        }

        if (end) {
            str->data = p;
            str->len = end - p;

        } else {
            str->data = p;
            str->len = last - p;

        }

        p += str->len + 1;
    }

#if (NGX_DEBUG)
    ngx_str_t  *arg;
    ngx_uint_t  i;

    arg = array->elts;
    for (i = 0; i < array->nelts; i++) {
        ngx_log_debug2(NGX_LOG_DEBUG_HTTP, ngx_cycle->log, 0,
                       "[flynx] res[%i]:%V", i, &arg[i]);
    }
#endif

    return array;
}

ngx_int_t
ngx_http_flynx_body_handler(ngx_http_request_t *r, ngx_http_client_body_handler_pt post_handler)
{
    ngx_int_t  rc;

    rc = ngx_http_read_client_request_body(r, post_handler);

    if (rc >= NGX_HTTP_SPECIAL_RESPONSE) {
        return rc;
    }

    return NGX_DONE;
}

ngx_int_t
ngx_http_flynx_finalize_response(ngx_http_request_t *r, ngx_buf_t *buffer) {
  return ngx_http_flynx_finalize_response_work(r, buffer, NGX_HTTP_OK);
}

ngx_int_t
ngx_http_flynx_finalize_response_work(ngx_http_request_t *r, ngx_buf_t *buffer, ngx_int_t status) {
  if(!buffer) {
    return NGX_HTTP_NO_CONTENT;
  }
  
  ngx_chain_t *chain = ngx_pcalloc(r->pool, sizeof(ngx_chain_t));
  buffer->memory = 1;
  buffer->last_buf = 1;
  
  chain->buf = buffer;
  chain->next = NULL;
  
  r->headers_out.status = status;
  r->headers_out.content_length_n = buffer->last - buffer->pos;
  r->headers_out.content_type.len = sizeof("application/json") - 1;
  r->headers_out.content_type.data = (u_char *) "application/json";
  ngx_http_send_header(r);
  
  return ngx_http_output_filter(r, chain);
}

ngx_int_t
ngx_http_flynx_finalize_response_array(ngx_http_request_t *r, ngx_array_t *buffers) {
  return ngx_http_flynx_finalize_response_array_work(r, buffers, NGX_HTTP_OK);
}

ngx_int_t
ngx_http_flynx_finalize_response_array_work(ngx_http_request_t *r, ngx_array_t *buffers, ngx_int_t status) {
  ngx_uint_t i;

  if(buffers->nelts == 0) {
    return NGX_HTTP_NO_CONTENT;
  }
  
  ngx_buf_t **bufs = buffers->elts;
  ngx_buf_t *buf;
    
  ngx_chain_t *first = ngx_pcalloc(r->pool, sizeof(ngx_chain_t));
  ngx_chain_t *local = first;
  
  ngx_int_t length = 0;
  
  for(i = 0; i < buffers->nelts; i++) {
    local->buf = bufs[i];
    buf = bufs[i];
    buf->memory = 1;
    
    length += (buf->last - buf->pos);
    
    if(i < buffers->nelts - 1) {
      ngx_chain_t *next = ngx_pcalloc(r->pool, sizeof(ngx_chain_t));
      local->next = next;
      local = next;
    } else {
      buf->last_buf = 1;
      local->next = NULL;
    }
  }
    
  r->headers_out.status = status;
  r->headers_out.content_length_n = length;
  r->headers_out.content_type.len = sizeof("application/json") - 1;
  r->headers_out.content_type.data = (u_char *) "application/json";
  ngx_http_send_header(r);

  return ngx_http_output_filter(r, first);
}

ngx_array_t * ngx_flynx_parse_uris(ngx_pool_t *pool, ngx_array_t *domains) {
  ngx_array_t *urls = ngx_array_create(pool, domains->nelts, sizeof(ngx_url_t));
  ngx_str_t *elts = domains->elts;
  ngx_str_t elt;
  ngx_url_t *u;
  ngx_uint_t n, i;
  
  ngx_int_t total_addrs = 0;
  
  for(n = 0; n < domains->nelts; n++) {
    elt = elts[n];
    
    u = ngx_array_push(urls);
    ngx_memzero(u, sizeof(ngx_url_t));

    u->url = elt;
    u->default_port = 80;

    if (ngx_parse_url(pool, u) != NGX_OK) {
        if (u->err) {
            ngx_log_error(NGX_LOG_ERR, ngx_cycle->log , 0,
                               "%s in url parse \"%V\"", u->err, &u->url);
        }
    }
    
    total_addrs += u->naddrs;
  }
  
  ngx_array_t *output = ngx_array_create(pool, total_addrs, sizeof(ngx_addr_t));
  u = urls->elts;
  
  for(n = 0; n < urls->nelts; n++) {
    ngx_url_t url = u[n];
    ngx_addr_t *addrs = url.addrs;
    ngx_addr_t addr;
    
    if(u->err) {
      continue;
    }
  
    for(i = 0; i < url.naddrs; i++) {
      addr = addrs[i];
      
      ngx_addr_t *current = ngx_array_push(output);
      *current = addr;
    }
  }
  
  return output;
}

ngx_array_t * ngx_flynx_parse_server_json(ngx_pool_t *pool, ngx_buf_t *buffer, ngx_int_t *version, ngx_flynx_data_peer_defaults_t *defaults) {
  json_t *json = NULL;
  json_t *json_servers = NULL;
  json_t *json_defaults = NULL;
  
  json_error_t error;
  
  json = json_loadb((char*)buffer->pos, buffer->last - buffer->pos, 0, &error);
  
  if(!json) {
    ngx_log_error(NGX_LOG_ERR, ngx_cycle->log, 0, "[flynx] json error: %s", &error.text);
    return NULL;
  }
  
  if(json_unpack_ex(json, &error, 0, "{s:i, s:O, s?O}", 
    "version", version, 
    "peers", &json_servers, 
    "defaults", &json_defaults)) {
    ngx_log_error(NGX_LOG_ERR, ngx_cycle->log, 0, "[flynx] json validation error: %s", &error.text);
    json_decref(json);
    return NULL;
  }
    
  size_t index;
  json_t *value;
  
  ngx_array_t *domains;
  
  if(json_is_array(json_servers)) {  
    domains = ngx_array_create(pool, json_array_size(json_servers), sizeof(ngx_str_t));
  
    json_array_foreach(json_servers, index, value) {
      if(json_is_string(value)) {
        ngx_str_t *cur = ngx_array_push(domains);
        const char *val = json_string_value(value);
        size_t len = ngx_strlen(val);
      
        cur->data = ngx_pcalloc(pool, len);
        ngx_memcpy(cur->data, (u_char*)val, len);
        cur->len = len; 
      } else {
        ngx_log_error(NGX_LOG_ERR, ngx_cycle->log, 0, "[flynx] found non-string value in array, skipping.");
      }
    }
  
  } else {
    ngx_log_error(NGX_LOG_ERR, ngx_cycle->log, 0, "[flynx] 'peers' is not an array");
    json_decref(json_servers);
    json_decref(json);
    
    return NULL;
  }
  
  if(defaults && json_defaults) {
    if(json_unpack_ex(json_defaults, &error, 0, "{s?i, s?i, s?i, s?i}", 
      "weight", &defaults->weight,
      "max_fails", &defaults->max_fails,
      "fail_timeout", &defaults->fail_timeout,
      "down", &defaults->down)) {
      ngx_log_error(NGX_LOG_ERR, ngx_cycle->log, 0, "[flynx] json validation error: %s", &error.text);
      json_decref(json_defaults);
    }
  }
    
  ngx_array_t *output = ngx_flynx_parse_uris(pool, domains);
  json_decref(json_servers);
  json_decref(json);
  
  return output;  
}

ngx_buf_t *ngx_http_flynx_json_to_buffer(ngx_pool_t *pool, json_t *json) {
  #if NGX_DEBUG
  char *output = json_dumps(json, JSON_INDENT(2));
#else
  char *output = json_dumps(json, 0);
#endif

  ngx_str_t str;
  str.data = ngx_pcalloc(pool, strlen(output));
  str.len = strlen(output);
  ngx_memcpy(str.data, output, strlen(output));
  
  free(output);
  
  ngx_buf_t *local = ngx_pcalloc(pool, sizeof(ngx_buf_t));
  local->pos = str.data;
  local->last = str.data + str.len;
  
  return local;
}

json_t *ngx_http_flynx_upstream_to_json(ngx_http_upstream_srv_conf_t *uscf) {
  json_t *json = json_object();  
  ngx_uint_t i;
  
  if(!uscf) {
    return NULL;
  }
  
  json_t *str = json_pack("s#", uscf->host.data, uscf->host.len);
  json_object_set_new(json, "name", str);
  
  json_t *array = json_array();
  json_object_set(json, "peers", array);
  
  ngx_http_upstream_rr_peers_t *peers = uscf->peer.data;
  
  for(i = 0; i < peers->number; i++) {
    json_t *local = json_object();
    ngx_http_upstream_rr_peer_t peer = peers->peer[i];
    
    str = json_pack("s#", peer.name.data, peer.name.len);
    json_object_set_new(local, "uri", str);
    json_object_set_new(local, "down", json_boolean(peer.down));
    json_object_set_new(local, "weight", json_integer(peer.weight));
    json_object_set_new(local, "max_fails", json_integer(peer.max_fails));
    json_object_set_new(local, "fail_timeout", json_integer(peer.fail_timeout));
    
    /*char buf[sizeof "2011-10-08T07:07:09Z"];
    strftime(buf, sizeof buf, "%FT%TZ", gmtime(&peer.accessed));
    json_object_set_new(local, "accessed", json_string(buf));
    strftime(buf, sizeof buf, "%FT%TZ", gmtime(&peer.checked));
    json_object_set_new(local, "checked", json_string(buf));
    strftime(buf, sizeof buf, "%FT%TZ", gmtime(&peer.fail_timeout));
    json_object_set_new(local, "fail_timeout", json_string(buf));*/
    
    json_array_append_new(array, local);
  }
  
  json_decref(array);
  
  return json;
}