#include "ngx_http_flynx_management_handlers.h"

#include "ngx_http_flynx_body.h"
#include "ngx_http_flynx_synchronisation.h"
#include "ngx_http_flynx_upstream.h"

#include <jansson.h>

ngx_int_t
ngx_http_flynx_ping(ngx_http_request_t *r) {  
  ngx_int_t version = ngx_flynx_global_ctx.sh->version;

  json_t *json = json_object();
  json_object_set_new(json, "status", version > 1 ? json_string("ok") : json_string("default"));
  json_object_set_new(json, "version", json_integer(version));
  
  ngx_buf_t *local = ngx_http_flynx_json_to_buffer(r->pool, json);
  json_decref(json);

  return ngx_http_flynx_finalize_response(r, local);
}

ngx_int_t
ngx_http_flynx_snapshot(ngx_http_request_t *r) {  
  ngx_http_upstream_srv_conf_t        **uscfp, *uscf;
  ngx_http_upstream_main_conf_t        *umcf;
  ngx_uint_t i;
  
  umcf = ngx_http_flynx_upstream_main_conf(r);
  uscfp = umcf->upstreams.elts;
  
  json_t *json = json_object();
  json_object_set_new(json, "version", json_integer((int)ngx_flynx_global_ctx.sh->version));
  json_t *inner = json_object();
  json_object_set(json, "upstreams", inner);
  
  for(i = 0; i < umcf->upstreams.nelts; i++) {
    uscf = uscfp[i];
    char buf[uscf->host.len + 1];
    ngx_memzero(buf, uscf->host.len + 1);
    ngx_memcpy(buf, uscf->host.data, uscf->host.len);
    json_object_set_new(inner, buf, ngx_http_flynx_upstream_to_json(uscf));
  }
  
  ngx_buf_t *local = ngx_http_flynx_json_to_buffer(r->pool, json);
  json_decref(json);

  return ngx_http_flynx_finalize_response(r, local);
}

void
ngx_http_flynx_upstream_peer_list_handler(ngx_http_request_t *r) { 
  ngx_int_t rc;
  ngx_str_t rv = ngx_null_string;
  ngx_buf_t buf;
  ngx_int_t result;
  ngx_buf_t *body;
  ngx_http_flynx_ctx_t *ctx;
  ngx_http_flynx_upstream_ctx_t *up;
  ngx_int_t version;
  
  
  if (r->request_body == NULL || r->request_body->bufs == NULL) {
      rc = NGX_HTTP_BAD_REQUEST;
      goto failed;
  }
  
  ctx = ngx_http_get_module_ctx(r, ngx_http_flynx_module);
  up = ctx->data;
  
  ngx_array_t *path = ctx->path;
  ngx_str_t *resource = path->elts;
  
  ngx_http_upstream_srv_conf_t *uscf = ngx_http_flynx_upstream_config(r, &resource[1]);
  
  if(!uscf) {
    rc = NGX_HTTP_NOT_FOUND;
    ngx_str_set(&rv, "{\"text\":\" could not find upstream.\"}");
    goto failed;
  }

  body = ngx_http_flynx_read_body(r);
  
  ngx_flynx_data_peer_defaults_t *defaults = ngx_pcalloc(r->pool, sizeof(ngx_flynx_data_peer_defaults_t));
  defaults->weight = 1;
  defaults->max_fails = 1;
  defaults->fail_timeout = 10;
  defaults->down = 0;
  
  ngx_array_t *addrs = ngx_flynx_parse_server_json(r->pool, body, &version, defaults);
  
  if(!addrs) {
    rc = NGX_HTTP_BAD_REQUEST;
    ngx_str_set(&rv, "{\"text\":\"could not resolve any addresses.\"}");
    goto failed;
  }
  
  if(addrs->nelts > 0) {    
    void *p;
    ngx_flynx_data_t *data;
    
    if(up->alloc) {
      p = up->alloc(r->pool);
    } else {
      p = ngx_pcalloc(r->pool, sizeof(ngx_flynx_data_peer_list_t));
    }
    
    if(!up->data) {
      rc = NGX_HTTP_BAD_REQUEST;
      ngx_str_set(&rv, "{\"text\":\"could not find data handler. is the endpoint valid?\"}");
      goto failed;
    }
    
    data = up->data(r->pool, ctx, version, p);
    
    ngx_flynx_data_peer_list_t *ap = p;
    ap->host = resource[1];
    ap->addrs = addrs->elts;
    ap->naddrs = addrs->nelts; 
    ap->defaults = defaults;
    
    if((result = ngx_http_flynx_send_msg(data))) {
      rc = NGX_HTTP_INTERNAL_SERVER_ERROR;
      ngx_str_set(&rv, "{\"text\":\"error while propagating change to workers.\"}");
      
      if(result == NGX_FLYNX_MANAGEMENT_VERSION_MISMATCH) {
        rc = NGX_HTTP_BAD_REQUEST;
        ngx_str_set(&rv, "{\"text\":\"version mismatch\"}");
      }
      goto failed;
    }
  }
  
  ngx_http_finalize_request(r, NGX_HTTP_NO_CONTENT);
  
  return;
  
failed:
  buf.pos = rv.data;
  buf.last = rv.data + rv.len;
  buf.in_file = 0;
  ngx_http_finalize_request(r, ngx_http_flynx_finalize_response_work(r, &buf, rc));
  
  return;
}

ngx_int_t
ngx_http_flynx_upstream_list(ngx_http_request_t *r) {  
  ngx_http_upstream_srv_conf_t        **uscfp, *uscf;
  ngx_http_upstream_main_conf_t        *umcf;
  ngx_uint_t i;
  
  umcf = ngx_http_flynx_upstream_main_conf(r);
  uscfp = umcf->upstreams.elts;
  
  json_t *json = json_object();
  json_object_set_new(json, "version", json_integer((int)ngx_flynx_global_ctx.sh->version));
  json_t *array = json_array();
  json_object_set(json, "upstreams", array);
  
  for(i = 0; i < umcf->upstreams.nelts; i++) {
    uscf = uscfp[i];    
    json_t *str = json_pack("s#", uscf->host.data, uscf->host.len);
    json_array_append_new(array, str);
  }
  
  ngx_buf_t *local = ngx_http_flynx_json_to_buffer(r->pool, json);
  json_decref(json);

  return ngx_http_flynx_finalize_response(r, local);
}

ngx_int_t
ngx_http_flynx_upstream_detail(ngx_http_request_t *r, ngx_str_t *host) {  
  json_t *json;
  
  ngx_http_upstream_srv_conf_t *uscf = ngx_http_flynx_upstream_config(r, host);
  
  if(uscf) {
    json = ngx_http_flynx_upstream_to_json(uscf);    
    json_object_set_new(json, "version", json_integer((int)ngx_flynx_global_ctx.sh->version));
  } else {
    return NGX_HTTP_NOT_FOUND;
  }
  
  ngx_buf_t *local = ngx_http_flynx_json_to_buffer(r->pool, json);
  json_decref(json);
  
  return ngx_http_flynx_finalize_response(r, local);
}

void* ngx_http_flynx_ctx_updown_alloc(ngx_pool_t *pool) {
  return ngx_pcalloc(pool, sizeof(ngx_flynx_data_updown_t));
}

ngx_flynx_data_t* ngx_http_flynx_ctx_add_data(ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p) {
  ngx_flynx_data_t *data = ngx_pcalloc(pool, sizeof(ngx_flynx_data_t));
  
  ngx_flynx_data_t d = ngx_flynx_data_add_peer(p, version);
  ngx_memcpy(data, &d, sizeof(ngx_flynx_data_t));
  
  return data;
}
ngx_flynx_data_t* ngx_http_flynx_ctx_delete_data(ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p) {
  ngx_flynx_data_t *data = ngx_pcalloc(pool, sizeof(ngx_flynx_data_t));
  
  ngx_flynx_data_t d = ngx_flynx_data_delete_peer(p, version);
  ngx_memcpy(data, &d, sizeof(ngx_flynx_data_t));
  
  return data;
}
ngx_flynx_data_t* ngx_http_flynx_ctx_replace_data(ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p) {
  ngx_flynx_data_t *data = ngx_pcalloc(pool, sizeof(ngx_flynx_data_t));
  
  ngx_flynx_data_t d = ngx_flynx_data_replace_peer(p, version);
  ngx_memcpy(data, &d, sizeof(ngx_flynx_data_t));
  
  return data;
}
ngx_flynx_data_t* ngx_http_flynx_ctx_updown_data(ngx_pool_t *pool, ngx_http_flynx_ctx_t *ctx, ngx_int_t version, void *p) {
  ngx_flynx_data_updown_t *updown = p;
  ngx_array_t *path = ctx->path;
  ngx_str_t *resource = path->elts;

  ngx_flynx_data_t *data = ngx_pcalloc(pool, sizeof(ngx_flynx_data_t));
  updown->down = *resource[2].data == 'd' ? 1 : 0;
  ngx_flynx_data_t d = ngx_flynx_data_updown(p, version);
  ngx_memcpy(data, &d, sizeof(ngx_flynx_data_t));
  
  return data;
}