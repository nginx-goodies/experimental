#include "ngx_http_flynx_common.h"
#include "ngx_http_flynx_synchronisation.h"

#include "ngx_http_flynx_upstream.h"

ngx_int_t
ngx_http_flynx_get_shm_name(ngx_str_t *shm_name, ngx_pool_t *pool,
    ngx_uint_t generation)
{
    u_char  *last;

    shm_name->data = ngx_palloc(pool, NGX_FLYNX_SHM_NAME_LEN);
    if (shm_name->data == NULL) {
        return NGX_ERROR;
    }

    last = ngx_snprintf(shm_name->data, NGX_FLYNX_SHM_NAME_LEN, "%s#%ui",
                        "ngx_http_flynx_moudule", generation);

    shm_name->len = last - shm_name->data;

    return NGX_OK;
}

char *
ngx_http_flynx_init_shm(ngx_conf_t *cf, void *conf) {
  ngx_http_flynx_main_conf_t *fmcf = conf;

  ngx_shm_zone_t  *shm_zone;

  if (ngx_http_flynx_get_shm_name(&fmcf->shm_name, cf->pool,
                                   ngx_http_flynx_generation)
      != NGX_OK)
  {
      return NGX_CONF_ERROR;
  }

  shm_zone = ngx_shared_memory_add(cf, &fmcf->shm_name, fmcf->shm_size,
                                   &ngx_http_flynx_module);
  if (shm_zone == NULL) {
      return NGX_CONF_ERROR;
  }

  ngx_log_error(NGX_LOG_DEBUG, cf->log, 0,
                "[flynx] init shm:%V, size:%ui", &fmcf->shm_name,
                fmcf->shm_size);

  shm_zone->data = cf->pool;
  shm_zone->init = ngx_http_flynx_init_shm_zone;

  return NGX_CONF_OK;
}

ngx_int_t
ngx_http_flynx_init_shm_zone(ngx_shm_zone_t *shm_zone, void *data)
{
    ngx_slab_pool_t    *shpool;
    ngx_flynx_shctx_t  *sh;

    shpool = (ngx_slab_pool_t *) shm_zone->shm.addr;

    sh = ngx_slab_alloc(shpool, sizeof(ngx_flynx_shctx_t));
    if (sh == NULL) {
        return NGX_ERROR;
    }

    ngx_flynx_global_ctx.sh = sh;
    ngx_flynx_global_ctx.shpool = shpool;

    ngx_queue_init(&sh->msg_queue);

    sh->version = 0; 

    return NGX_OK;
}

void 
ngx_http_flynx_timer_callback(ngx_event_t *ev) {
  ngx_slab_pool_t *shpool;
  
  shpool = ngx_flynx_global_ctx.shpool;
  
  ngx_shmtx_trylock(&shpool->mutex);
  ngx_http_flynx_timer_callback_locked(ev);
  ngx_shmtx_unlock(&shpool->mutex);
    
  ngx_flynx_add_timer(ev, 1000);
}

void
ngx_http_flynx_timer_callback_locked(ngx_event_t *ev) {
  ngx_slab_pool_t     *shpool;
  ngx_flynx_msg_t     *msg;
  ngx_flynx_shctx_t   *sh;
  
  ngx_queue_t         *q, *t;
  ngx_core_conf_t     *ccf;
  
  ngx_int_t found = 0;
  
  ccf = (ngx_core_conf_t *) ngx_get_conf(ngx_cycle->conf_ctx,
                                           ngx_core_module);

  sh = ngx_flynx_global_ctx.sh;
  shpool = ngx_flynx_global_ctx.shpool;
  
  if (ngx_queue_empty(&sh->msg_queue)) {
      return;
  }
  
  for (q = ngx_queue_last(&sh->msg_queue);
       q != ngx_queue_sentinel(&sh->msg_queue);
       q = ngx_queue_prev(q)) {
    
    msg = ngx_queue_data(q, ngx_flynx_msg_t, queue);
    
    if (msg->count == ccf->worker_processes) {
      t = ngx_queue_next(q); ngx_queue_remove(q); q = t;
      
      ngx_http_flynx_destroy_msg_locked(shpool, msg);
      continue;
    }
    
    int i;
    for(i = 0; i < msg->count; i++) {
      if(msg->pid[i] == ngx_pid) {
        found = 1;
        break;
      }
    }
    
    if(found) {
      continue;
    }   
    
    msg->pid[i] = ngx_pid;
    msg->count++;
    
    if(msg->content.handler == NULL) {
      ngx_log_error(NGX_LOG_WARN, ngx_cycle->log, 0, "no handler attached for event type %d", msg->content.type);
    } else {
      msg->content.handler(msg->content.data);
    }
  }
  
  return;
}

ngx_int_t
ngx_http_flynx_send_msg(ngx_flynx_data_t *content) {
  return ngx_http_flynx_send_msg_with_cycle(content, ngx_cycle);
}

ngx_int_t
ngx_http_flynx_send_msg_with_cycle(ngx_flynx_data_t *content, volatile ngx_cycle_t *cycle) {
  ngx_core_conf_t    *ccf;
  ngx_slab_pool_t    *shpool;
  ngx_flynx_msg_t    *msg;
  ngx_flynx_shctx_t  *sh;
  
  ccf = (ngx_core_conf_t *) ngx_get_conf(cycle->conf_ctx,
                                         ngx_core_module);

  sh = ngx_flynx_global_ctx.sh;
  shpool = ngx_flynx_global_ctx.shpool;
  
  ngx_shmtx_lock(&shpool->mutex);
  
  if(sh->version >= content->version) {
    ngx_shmtx_unlock(&shpool->mutex);
    return NGX_FLYNX_MANAGEMENT_VERSION_MISMATCH;
  }
  
  sh->version = content->version;

  
  msg = ngx_slab_alloc_locked(shpool, sizeof(ngx_flynx_msg_t));
  
  ngx_shmtx_unlock(&shpool->mutex);
  
  if (msg == NULL) {
      goto failed;
  } 
 
  ngx_memzero(msg, sizeof(ngx_flynx_msg_t));
  
  msg->pid = ngx_slab_alloc_locked(shpool, sizeof(ngx_pid_t) * ccf->worker_processes);
  
  if(msg->pid == NULL) {
    goto failed;
  } 
  
  ngx_memzero(msg->pid, sizeof(ngx_pid_t) * ccf->worker_processes);
  
  if(content) {
    if(!!content->alloc != !!content->free) {
      ngx_log_error(NGX_LOG_ERR, cycle->log, 0, "ngx_flynx_data_t does not have both alloc and free pts");
      goto failed;
    }
  
    if(content->data && content->len > 0) {
      msg->content.type = content->type;
      
      ngx_shmtx_lock(&shpool->mutex);
      msg->content.data = ngx_slab_alloc_locked(shpool, content->len);
      ngx_shmtx_unlock(&shpool->mutex);
      
      ngx_memcpy(msg->content.data, content->data, content->len);
      msg->content.len = content->len;
    
      if(content->alloc) {
      
        ngx_shmtx_lock(&shpool->mutex);
        content->alloc(shpool, content->data, msg->content.data);
        ngx_shmtx_unlock(&shpool->mutex);
        
      }
    }
    
    msg->content.handler = content->handler;
    msg->content.alloc = content->alloc;
    msg->content.free = content->free;
  }
  
  ngx_queue_insert_head(&sh->msg_queue, &msg->queue);
  
  return NGX_OK;
  
failed:
  if (msg) {
      ngx_http_flynx_destroy_msg(shpool, msg);
  }

  return NGX_ERROR;
}

void
ngx_http_flynx_destroy_msg(ngx_slab_pool_t *shpool, ngx_flynx_msg_t *msg) {
  ngx_shmtx_lock(&shpool->mutex);
  ngx_http_flynx_destroy_msg_locked(shpool, msg);
  ngx_shmtx_unlock(&shpool->mutex);
}

void
ngx_http_flynx_destroy_msg_locked(ngx_slab_pool_t *shpool, ngx_flynx_msg_t *msg) {  
  if(msg->pid) {
    ngx_slab_free_locked(shpool, msg->pid);
  }
  
  if(msg->content.data) {
    if(msg->content.free) {
      msg->content.free(shpool, msg->content.data);
    }
    ngx_slab_free_locked(shpool, msg->content.data);
  }
  
  ngx_slab_free_locked(shpool, msg);
}

ngx_int_t
ngx_http_flynx_data_init_handler(void *data) {  
  return 1;
}