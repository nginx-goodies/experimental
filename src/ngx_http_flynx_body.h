#ifndef _NGX_HTTP_FLYNX_BODY_INCLUDED_
#define _NGX_HTTP_FLYNX_BODY_INCLUDED_

#include "ngx_http_flynx_common.h"
#include <jansson.h>

ngx_buf_t * ngx_http_flynx_read_body(ngx_http_request_t *r);
ngx_buf_t * ngx_http_flynx_read_body_from_file(ngx_http_request_t *r);
ngx_array_t * ngx_flynx_parse_body(ngx_pool_t *pool, ngx_buf_t *input);
ngx_array_t * ngx_flynx_parse_path(ngx_pool_t *pool, ngx_str_t *path, ngx_str_t *loc_path);
ngx_array_t * ngx_flynx_parse_single_path(ngx_pool_t *pool, ngx_str_t *path);
ngx_int_t ngx_http_flynx_finalize_response(ngx_http_request_t *r, ngx_buf_t *buffer);
ngx_int_t ngx_http_flynx_finalize_response_work(ngx_http_request_t *r, ngx_buf_t *buffer, ngx_int_t status);
ngx_int_t ngx_http_flynx_finalize_response_array(ngx_http_request_t *r, ngx_array_t *buffers);
ngx_int_t ngx_http_flynx_finalize_response_array_work(ngx_http_request_t *r, ngx_array_t *buffers, ngx_int_t status);
ngx_int_t ngx_http_flynx_body_handler(ngx_http_request_t *r, ngx_http_client_body_handler_pt post_handler);

ngx_array_t * ngx_flynx_parse_uris(ngx_pool_t *pool, ngx_array_t *domains);
ngx_array_t * ngx_flynx_parse_server_json(ngx_pool_t *pool, ngx_buf_t *buffer, ngx_int_t *version, ngx_flynx_data_peer_defaults_t *defaults);
ngx_buf_t *ngx_http_flynx_json_to_buffer(ngx_pool_t *pool, json_t *json);
json_t *ngx_http_flynx_upstream_to_json(ngx_http_upstream_srv_conf_t *uscf);

#endif //_NGX_HTTP_FLYNX_BODY_INCLUDED_